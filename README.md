# Runeterra Match Viewer

  <p align="center">
    An Android application to watch your sessions of Legends of Runeterra by Thierry LAM.
    <i>Legends of Runeterra (LoR) is a one versus one player, free-to-play digital collectible card game developed and published by Riot Games. The game uses characters and a setting originating in League of Legends, a multiplayer online battle arena game by Riot Games. </i>
  </p>


<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

This project uses the API granted from Riot Games for their digital collectible card game "Legends of Runeterra" to summarize the latest duels.

Here's why:
* If a player couldn't take advantage of his opponent then he can watch his deck card per card to know his flaws and adapt his strategy.
* If a player liked what he fight then he can launch our app and import the opponent deck directly in game.
* If a player is in a successful win-streak and he wants to share his exploit then he can hit the share button and favorite to save them.

I hope this will serve you well!

### Built With

The project follows the MvvM architecture with:
* [Kotlin](https://kotlinlang.org/)
* [Koin](https://insert-koin.io/)
* [Retrofit](https://square.github.io/retrofit/)
* [Okhttp](https://square.github.io/okhttp/)
* [Coroutine](https://kotlinlang.org/docs/reference/coroutines-overview.html)
* Arrow-core ([Either](https://arrow-kt.io/docs/apidocs/arrow-core-data/arrow.core/-either/))
* [Stelar7 R4J](https://github.com/stelar7/R4J) (deck decoder)
* Some UI tools: [Cardview](https://developer.android.com/guide/topics/ui/layout/cardview), [RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview), [ChromeTab](https://developers.google.com/web/android/custom-tabs/implementation-guide)


## Getting Started

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/thierrylam2408/runeterra-match-viewer.git
   ```
2. Register your product and get your API Key to [LoR developer website](https://developer.riotgames.com/docs/lor)
3. Enter your API key in `secrets.properties` at the <b>root</b> of your project
   ```JS
   X_RIOT_TOKEN_API_KEY = 'ENTER YOUR API';
   ```
4. Build the apk
  ```sh
  ./gradlew assembleDebug
  ```
OR you can download [artifacts](https://gitlab.com/thierrylam2408/runeterra-match-viewer/-/pipelines) on Gitlab

## Usage

You can see below, an example of browsing, (filtering matches list, showing the details of a match, testing screen rotation, copy a deck and looking for my opponent latest matches).
You can try on your own with "Sorreil" as <b>Player name</b> and "Euw" as <b>Server name</b>.

![Runeterra Match Viewer Demo](demo.gif)


<!-- CONTACT -->
## Contact

Thierry LAM - thierrylam2408@gmail.com
