package com.example.runeterramatchviewer

import android.app.Application
import com.example.runeterramatchviewer.di.RuneterraMatchViewerComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RuneterraMatchViewerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            printLogger()
            androidContext(this@RuneterraMatchViewerApplication)
            modules(
                    RuneterraMatchViewerComponent().networkModule,
                    RuneterraMatchViewerComponent().appModule,
                    RuneterraMatchViewerComponent().playerIdentificationModule,
                    RuneterraMatchViewerComponent().matchesHistoryModule,
                    RuneterraMatchViewerComponent().matchDetailModule
            )
        }
    }
}