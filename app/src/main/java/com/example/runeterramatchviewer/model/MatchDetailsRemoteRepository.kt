package com.example.runeterramatchviewer.model

import arrow.core.Either


interface MatchDetailsRemoteRepository {
    fun getMatchDetail(matchId: String): Either<AppError, MatchDetail>
    fun getPlayerName(playerId: String): Either<AppError, PlayerInfo>
}

