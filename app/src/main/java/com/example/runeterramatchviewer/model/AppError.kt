package com.example.runeterramatchviewer.model

sealed class AppError

const val SUCCESS_CODE = 200
const val PLAYER_NOT_FOUND_CODE = 404
const val SERVICE_UNAVAILABLE = 503

object NoNetworkError : AppError()
data class HttpError(val code: Int) : AppError()
object UnprocessedNetworkError : AppError()
object InputError : AppError()
