package com.example.runeterramatchviewer.model

data class PlayerInfo(val playerId: String, val name: String)