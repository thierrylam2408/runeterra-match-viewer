package com.example.runeterramatchviewer.model

import arrow.core.Either

interface MatchesIdRepository {
    fun getMatchesId(playerId: String): Either<AppError, List<String>>
}
