package com.example.runeterramatchviewer.model

import arrow.core.Either


interface MatchDetailsLocalRepository {
    fun isPresent(matchId: String): Boolean
    fun persist(match: MatchDetail)
    fun getMatchDetail(matchId: String): MatchDetail
    fun getMatchDetailOrNull(matchId: String): Either<AppError, MatchDetail?>
}