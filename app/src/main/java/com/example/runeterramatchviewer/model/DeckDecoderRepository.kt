package com.example.runeterramatchviewer.model

interface DeckDecoderRepository {

    fun decode(code: String): Deck

}