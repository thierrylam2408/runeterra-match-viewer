package com.example.runeterramatchviewer.model

import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.utils.ResourcesUtils

data class MatchDetail(
        val idMatch: String,
        val gameMode: ModeName,
        val gameType: TypeMatchName,
        val winner: PlayerDetail,
        val loser: PlayerDetail,
        val totalTurn: Int,
        val dateStart: String
) {
    fun getPlayerOutcome(playerId: String): Outcome {
        return if (winner.id == playerId) {
            Outcome.WINNER
        } else return Outcome.LOSER
    }

    fun getPlayerDetail(playerId: String): PlayerDetail {
        return if (winner.id == playerId) {
            winner
        } else loser
    }

    fun getOtherPlayerDetail(playerId: String): PlayerDetail {
        return if (winner.id != playerId) {
            winner
        } else loser
    }
}

data class PlayerDetail(val id: String, val factions: List<RegionName>, val deckCode: String)


enum class ModeName {
    CONSTRUCTED, EXPEDITION, GAUNTLET, ALL;

    companion object {
        fun fromDisplayName(displayName: String, utils: ResourcesUtils): ModeName {
            return when (displayName) {
                utils.getString(R.string.constructed) -> CONSTRUCTED
                utils.getString(R.string.expedition) -> EXPEDITION
                utils.getString(R.string.gauntlet) -> GAUNTLET
                else -> ALL
            }
        }
    }
}

enum class TypeMatchName {
    NORMAL, RANKED, ALL;

    companion object {
        fun fromDisplayName(displayName: String, utils: ResourcesUtils): TypeMatchName {
            return when (displayName) {
                utils.getString(R.string.normal) -> NORMAL
                utils.getString(R.string.ranked) -> RANKED
                else -> ALL
            }
        }
    }
}

enum class Outcome {
    WINNER, LOSER
}