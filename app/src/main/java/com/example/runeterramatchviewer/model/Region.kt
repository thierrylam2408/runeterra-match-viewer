package com.example.runeterramatchviewer.model

enum class RegionName {
    BILGEWATER, DEMACIA, FRELJORD, IONIA, NOXUS, PILTOVERZAUN, SHADOWISLES, TARGON, UNKNOWN
}