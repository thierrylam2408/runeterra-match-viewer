package com.example.runeterramatchviewer.model

data class Deck(val cards: Map<Card, Int>)

data class Card(val name: String, val region: RegionName, val type: TypeCardName, val rarity: RarityName)

enum class TypeCardName {
    UNIT, SPELL, LANDMARK, NONE
}

enum class RarityName {
    CHAMPION, EPIC, RARE, COMMON, NONE
}