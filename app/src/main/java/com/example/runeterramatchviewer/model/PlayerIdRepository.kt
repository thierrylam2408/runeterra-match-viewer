package com.example.runeterramatchviewer.model

import arrow.core.Either

interface PlayerIdRepository {
    fun getPlayerId(playerName: String, tagName: String): Either<AppError, String>
}