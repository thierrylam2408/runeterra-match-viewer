package com.example.runeterramatchviewer.interactor

import arrow.core.Either
import arrow.core.handleErrorWith
import arrow.core.leftIfNull
import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.repository.DeckDecoderJsonReaderRepository

class MatchesHistoryInteractor(
        private val matchesIdRepository: MatchesIdRepository,
        private val matchDetailsRemoteRepository: MatchDetailsRemoteRepository,
        private val matchDetailsLocalRepository: MatchDetailsLocalRepository,
        private val deckDecoder: DeckDecoderJsonReaderRepository
) {

    fun getMatches(playerId: String): Either<AppError, List<MatchDetail>> {
        val numberMatches = 10 //adjust to avoid rate limiter
        val matchesList = this.matchesIdRepository.getMatchesId(playerId)
                .map { list ->
                    list.subList(0, numberMatches.coerceAtMost(list.size))
                            .mapNotNull { getMatchById(it).orNull() }
                }
        matchesList.orNull()?.forEach { matchDetailsLocalRepository.persist(it) }
        return matchesList
    }

    fun getMatchById(matchId: String): Either<AppError, MatchDetail> {
        return matchDetailsLocalRepository.getMatchDetailOrNull(matchId)
                .leftIfNull { InputError }
                .handleErrorWith { matchDetailsRemoteRepository.getMatchDetail(matchId) }
    }

    fun getPlayerName(puuid: String): Either<AppError, PlayerInfo> {
        return this.matchDetailsRemoteRepository.getPlayerName(puuid)
    }

    fun getDeck(code: String): Deck {
        return deckDecoder.decode(code)
    }
}