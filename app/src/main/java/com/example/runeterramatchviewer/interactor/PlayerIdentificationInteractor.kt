package com.example.runeterramatchviewer.interactor

import arrow.core.Either
import com.example.runeterramatchviewer.model.AppError
import com.example.runeterramatchviewer.model.PlayerIdRepository

class PlayerIdentificationInteractor(
        private val playerIdRepository: PlayerIdRepository
) {

    fun getPlayerId(playerName: String, tagName: String): Either<AppError, String> {
        return playerIdRepository.getPlayerId(playerName, tagName)
    }
}