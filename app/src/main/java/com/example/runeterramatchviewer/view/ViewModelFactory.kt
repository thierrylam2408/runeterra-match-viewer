package com.example.runeterramatchviewer.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.runeterramatchviewer.interactor.MatchesHistoryInteractor
import com.example.runeterramatchviewer.interactor.PlayerIdentificationInteractor
import com.example.runeterramatchviewer.utils.ResourcesUtils

class ViewModelFactory(
        private val playerIdentificationInteractor: PlayerIdentificationInteractor,
        private val matchesHistoryInteractor: MatchesHistoryInteractor,
        private val resourcesUtils: ResourcesUtils
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlayerIdViewModel::class.java)) {
            return PlayerIdViewModel(playerIdentificationInteractor, resourcesUtils) as T
        } else if (modelClass.isAssignableFrom(MatchesHistoryViewModel::class.java)) {
            return MatchesHistoryViewModel(matchesHistoryInteractor, resourcesUtils) as T
        } else if (modelClass.isAssignableFrom(MatchDetailViewModel::class.java)) {
            return MatchDetailViewModel(matchesHistoryInteractor, resourcesUtils) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}