package com.example.runeterramatchviewer.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.interactor.PlayerIdentificationInteractor
import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.utils.ConsumableEvent
import com.example.runeterramatchviewer.utils.ResourcesUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PlayerIdViewModel(
        private val playerIdentificationInteractor: PlayerIdentificationInteractor,
        private val resourcesUtils: ResourcesUtils
) : ViewModel() {

    private var playerName: MutableLiveData<String> = MutableLiveData()
    private var tagName: MutableLiveData<String> = MutableLiveData()
    private var notice: MutableLiveData<ConsumableEvent> = MutableLiveData()
    private var id: MutableLiveData<ConsumableEvent> = MutableLiveData()

    fun startGetPlayerId() {
        viewModelScope.launch {
            getPlayerId()
        }
    }

    suspend fun getPlayerId() {
        if (playerName.value == null || tagName.value == null ||
                playerName.value!!.trim() == "" || tagName.value!!.trim() == "") {
            this.notice.value = ConsumableEvent(resourcesUtils.getString(R.string.input_error))
            return
        }
        val result = withContext(Dispatchers.IO) {
            playerIdentificationInteractor.getPlayerId(playerName.value!!, tagName.value!!)
        }
        when (result) {
            is Either.Left -> when (result.a) {
                is HttpError -> {
                    val errorData = (result.a as HttpError)
                    when (errorData.code) {
                        PLAYER_NOT_FOUND_CODE ->
                            this.notice.value =
                                    ConsumableEvent(
                                            resourcesUtils.getString(
                                                    R.string.player_not_found,
                                                    playerName.value!!,
                                                    tagName.value!!
                                            ))
                    }

                }
                is InputError -> this.notice.value =
                        ConsumableEvent(resourcesUtils.getString(R.string.input_error))
                is NoNetworkError ->
                    this.notice.value = ConsumableEvent(
                            resourcesUtils.getString(
                                    R.string.no_internet
                            )
                    )
                is UnprocessedNetworkError -> this.notice.value =
                        ConsumableEvent(resourcesUtils.getString(R.string.network_error))
            }
            is Either.Right -> this.id.value = ConsumableEvent(result.b)
        }
    }

    fun editPlayerName(newName: String) {
        this.playerName.value = newName
    }

    fun editTagName(newTag: String) {
        this.tagName.value = newTag
    }

    fun getPlayerName(): LiveData<String> {
        return this.playerName
    }

    fun getTagName(): LiveData<String> {
        return this.tagName
    }

    fun getNotice(): LiveData<ConsumableEvent> {
        return this.notice
    }

    fun getId(): LiveData<ConsumableEvent> {
        return this.id
    }

    fun consumeNotice() {
        this.notice.value = this.notice.value?.copy(consume = true)
    }

    fun consumeActivity() {
        this.id.value = this.id.value?.copy(consume = true)
    }

}