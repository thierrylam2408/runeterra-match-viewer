package com.example.runeterramatchviewer.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.runeterramatchviewer.databinding.ActivityPlayerIdentificationBinding
import com.example.runeterramatchviewer.di.RuneterraMatchViewerComponent
import com.example.runeterramatchviewer.utils.ConsumableEvent
import com.google.android.material.snackbar.Snackbar


const val EXTRA_PLAYER_ID = "EXTRA_PLAYER_ID"

class PlayerIdentificationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlayerIdentificationBinding
    private lateinit var viewModel: PlayerIdViewModel
    private val viewModelFactory = RuneterraMatchViewerComponent().viewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayerIdentificationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel =
                ViewModelProvider(this, viewModelFactory).get(PlayerIdViewModel::class.java)

        binding.playerNameInputLayout.editText!!.addTextChangedListener(textWatcher(EditType.PLAYER))
        binding.tagNameInputLayout.editText!!.addTextChangedListener(textWatcher(EditType.TAG))
        binding.nextButton.setOnClickListener(identificationRequestListener())
        binding.runeterraWebsiteButton.setOnClickListener(playRuneterraRequestListener())


        viewModel.getNotice().observe(this, noticeObserver())
        viewModel.getPlayerName().observe(this, { playerName ->
            if (playerName != binding.playerNameInputEdit.text.toString())
                binding.playerNameInputEdit.setText(playerName)
        })
        viewModel.getTagName().observe(this, { tagName ->
            if (tagName != binding.tagNameInputEdit.text.toString())
                binding.tagNameInputEdit.setText(tagName)
        })
        viewModel.getId().observe(this, { id ->
            val startLoginEvent = viewModel.getId().value
            if (!startLoginEvent!!.consume) {
                val intent = Intent(this, MatchesHistoryActivity::class.java).apply {
                    putExtra(EXTRA_PLAYER_ID, startLoginEvent.message)
                }
                viewModel.consumeActivity()
                startActivity(intent)
            }
        })
    }

    private fun noticeObserver(): Observer<ConsumableEvent> {
        return Observer<ConsumableEvent> { notice ->
            if (!notice.consume) {
                Snackbar.make(
                        binding.root,
                        notice.message,
                        Snackbar.LENGTH_SHORT
                ).show()
                viewModel.consumeNotice()
            }
        }
    }

    private fun textWatcher(editType: EditType): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                val newText = s.toString()
                when (editType) {
                    EditType.PLAYER -> {
                        if (newText == viewModel.getPlayerName().value) return
                        viewModel.editPlayerName(s.toString())
                    }
                    EditType.TAG -> {
                        if (newText == viewModel.getTagName().value) return
                        viewModel.editTagName(s.toString())
                    }
                }

            }
        }
    }

    private fun identificationRequestListener(): View.OnClickListener {
        return View.OnClickListener { view -> viewModel.startGetPlayerId() }
    }

    private fun playRuneterraRequestListener(): View.OnClickListener {
        return View.OnClickListener {
            val url = "market://details?id=com.riotgames.legendsofruneterra"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    enum class EditType {
        PLAYER, TAG
    }
}