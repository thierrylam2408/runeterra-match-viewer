package com.example.runeterramatchviewer.view

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.databinding.ActivityMatchDetailBinding
import com.example.runeterramatchviewer.databinding.CustomDeckTabBinding
import com.example.runeterramatchviewer.databinding.CustomRegionCardBinding
import com.example.runeterramatchviewer.di.RuneterraMatchViewerComponent
import com.example.runeterramatchviewer.model.Outcome
import com.example.runeterramatchviewer.model.PlayerDetail
import com.example.runeterramatchviewer.model.RarityName
import com.example.runeterramatchviewer.model.TypeCardName
import com.example.runeterramatchviewer.utils.AssetsMatcher
import com.example.runeterramatchviewer.utils.DisplayFormat
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener


class MatchDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMatchDetailBinding
    private lateinit var viewModel: MatchDetailViewModel
    private val viewModelFactory = RuneterraMatchViewerComponent().viewModelFactory
    private lateinit var assetsMatcher: AssetsMatcher
    private val customTabsIntentBuilder = CustomTabsIntent.Builder()
    private val baseUrlDeckCode = "https://dekki.com/en/games/legends-of-runeterra/deck-builder?code="

    //    private val baseUrlDeckCode = "https://dak.gg/lor/builder?deckCode="
    private lateinit var clipboardManager: ClipboardManager
    private lateinit var tabsDeck: List<CustomDeckTabBinding>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMatchDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        tabsDeck = listOf(binding.tab2, binding.tab3)
        assetsMatcher = AssetsMatcher(application.assets)
        clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        viewModel = ViewModelProvider(this, viewModelFactory)
                .get(MatchDetailViewModel::class.java)
        val matchId = intent.getStringExtra(EXTRA_MATCH_ID)
        val currentPlayerId = intent.getStringExtra(EXTRA_PLAYER_ID)
        if (savedInstanceState == null) {
            this.viewModel.startInitViewModel(matchId!!, currentPlayerId!!)
        }

        viewModel.getMatchDetails().observe(this, matchObserver(currentPlayerId!!))

        viewModel.getLoading().observe(this, loadingObserver())

        viewModel.getNotice().observe(this, { binding.noticeText.text = it })

        viewModel.getTabPosition().observe(this, tabPositionObserver())

        binding.tabMatch.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewModel.setTabPosition(tab!!.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })

        viewModel.getFavorite().observe(this, {
            val icon = if (it) {
                R.drawable.ic_baseline_favorite_24
            } else {
                R.drawable.ic_baseline_favorite_border_24
            }
            binding.favorite.setIconResource(icon)
        })


        binding.favorite.setOnClickListener {
            //TODO save idMatch in bookmark
            viewModel.setFavorite(!viewModel.getFavorite().value!!)
        }

        binding.share.setOnClickListener {
            //TODO listener for share button
        }
    }

    private fun bindExpandsButtons(regionsP1: List<CustomRegionCardBinding>,
                                   regionsP2: List<CustomRegionCardBinding>) {
        val regions = listOf(regionsP1, regionsP2)
        for (regionsCard in regions) {
            val playerIndex = regions.indexOf(regionsCard)
            for (i in 0 until viewModel.getNumberPanels(playerIndex)) {
                viewModel.getExpandBoolean(playerIndex, i)?.observe(this, {
                    if (it) {
                        regionsCard.getOrNull(i)?.regionList?.visibility = VISIBLE
                        regionsCard.getOrNull(i)?.regionArrowImg?.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
                    } else {
                        regionsCard.getOrNull(i)?.regionList?.visibility = GONE
                        regionsCard.getOrNull(i)?.regionArrowImg?.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
                    }
                })
            }

        }
    }

    private fun tabPositionObserver(): Observer<Int> {
        return Observer {
            if (it != binding.tabMatch.selectedTabPosition) {
                binding.tabMatch.getTabAt(it)!!.view.performClick()
            }
            when (it) {
                0 -> {
                    binding.tab1.visibility = VISIBLE
                    binding.tab2.root.visibility = GONE
                    binding.tab3.root.visibility = GONE
                }
                1 -> {
                    binding.tab1.visibility = GONE
                    binding.tab2.root.visibility = VISIBLE
                    binding.tab3.root.visibility = GONE
                }
                else -> {
                    binding.tab1.visibility = GONE
                    binding.tab2.root.visibility = GONE
                    binding.tab3.root.visibility = VISIBLE
                }
            }
        }
    }

    private fun matchObserver(currentPlayerId: String):
            Observer<MatchDetailViewModel.MatchDetailsAndPlayersName> {
        return Observer {
            val currentPlayer = it.matchDetails.getPlayerDetail(currentPlayerId)
            val otherPlayer = it.matchDetails.getOtherPlayerDetail(currentPlayerId)

            bindMatchTab(it, currentPlayer, otherPlayer)

            val indexP1 = 0
            val regionsCardP1 = bindDeckTab(currentPlayer, tabsDeck[indexP1], indexP1)

            val indexP2 = 1
            val regionsCardP2 = bindDeckTab(otherPlayer, tabsDeck[indexP2], indexP2)

            bindExpandsButtons(regionsCardP1, regionsCardP2)
        }
    }

    private fun bindDeckTab(player: PlayerDetail, tab: CustomDeckTabBinding, index: Int)
            : List<CustomRegionCardBinding> {
        val currentDeck = viewModel.getDeck(player.deckCode)
        val commons = currentDeck.cards.filterKeys { it.rarity == RarityName.COMMON }.map { it.value }.sum()
        val rares = currentDeck.cards.filterKeys { it.rarity == RarityName.RARE }.map { it.value }.sum()
        val epic = currentDeck.cards.filterKeys { it.rarity == RarityName.EPIC }.map { it.value }.sum()
        val champions = currentDeck.cards.filterKeys { it.rarity == RarityName.CHAMPION }.map { it.value }.sum()
        val units = currentDeck.cards.filterKeys { it.type == TypeCardName.UNIT }.map { it.value }.sum()
        val spells = currentDeck.cards.filterKeys { it.type == TypeCardName.SPELL }.map { it.value }.sum()
        val landmarks = currentDeck.cards.filterKeys { it.type == TypeCardName.LANDMARK }.map { it.value }.sum()
        val total = currentDeck.cards.map { it.value }.sum()
        tab.deckInfoLeft.text = getString(R.string.deck_info_numbers, units, spells, landmarks, total)
        tab.deckInfoRight.text = getString(R.string.deck_info_numbers, champions, epic, rares, commons)

        val regionsCardView = mutableListOf<CustomRegionCardBinding>()
        for (i in player.factions.indices) {
            val faction = player.factions[i]
            val regionCardView = CustomRegionCardBinding.inflate(LayoutInflater.from(this))
            tab.parentRegionsCards.addView(regionCardView.root)
            regionCardView.regionIcon.setImageBitmap(
                    assetsMatcher.loadRegionIcon(faction)
            )
            regionCardView.regionName.text = DisplayFormat.capitalize(faction.name)

            regionCardView.regionPanel.setOnClickListener { viewModel.toggleExpandBoolean(index, i) }
            val filterRegion = currentDeck.cards
                    .filterKeys { it.region == faction }
                    .toSortedMap(compareBy { it.name })
            regionCardView.regionNumber.text =
                    getString(
                            R.string.cards_per_region,
                            filterRegion.map { it.value }.sum(),
                            total
                    )
            var cardsRegion = ""
            filterRegion.forEach {
                cardsRegion += "${it.value}x ${it.key.name}\n"
            }
            regionCardView.regionList.text = cardsRegion
            regionsCardView.add(regionCardView)
        }

        tab.playerCodeLink.setOnClickListener {
            val customTabsIntent: CustomTabsIntent = customTabsIntentBuilder.build();
            customTabsIntent.launchUrl(this,
                    Uri.parse(baseUrlDeckCode + player.deckCode))
        }
        tab.playerCodeCopy.setOnClickListener {
            val clipData = ClipData.newPlainText("Deck Code", player.deckCode)
            clipboardManager.setPrimaryClip(clipData)
            Snackbar.make(binding.root,
                    getString(R.string.clipboard_deck_success, player.deckCode),
                    Snackbar.LENGTH_SHORT).show()
        }
        return regionsCardView
    }

    private fun bindMatchTab(
            it: MatchDetailViewModel.MatchDetailsAndPlayersName,
            currentPlayer: PlayerDetail,
            otherPlayer: PlayerDetail) {
        binding.dateStart.text = getString(
                R.string.dateStart,
                DisplayFormat.dateFormat(it.matchDetails.dateStart)
        )
        binding.gameMode.text = getString(
                R.string.mode,
                DisplayFormat.capitalize(it.matchDetails.gameMode.name)
        )
        binding.gameType.text = getString(
                R.string.type,
                DisplayFormat.capitalize(it.matchDetails.gameType.name)
        )
        binding.totalTurn.text = getString(
                R.string.turn,
                it.matchDetails.totalTurn
        )
        binding.outcome.text = getString(
                R.string.outcome,
                DisplayFormat.capitalize(
                        it.matchDetails.getPlayerOutcome(currentPlayer.id).name)
        )

        binding.matchesOpponent.text = it.playersName[otherPlayer.id]
        binding.matchesOpponent.setOnClickListener {
            val intent = Intent(this, MatchesHistoryActivity::class.java).apply {
                putExtra(EXTRA_PLAYER_ID, otherPlayer.id)
            }
            startActivity(intent)
        }

        val colorOutcome = if (it.matchDetails.getPlayerOutcome(currentPlayer.id) == Outcome.WINNER) {
            R.color.card_win_background
        } else {
            R.color.card_loss_background
        }
        binding.colorOutcome.setBackgroundResource(colorOutcome)

        binding.player1Name.text = getString(
                R.string.player1,
                it.playersName[currentPlayer.id]
        )
        binding.player2Name.text = getString(
                R.string.player2,
                it.playersName[otherPlayer.id]
        )
    }

    private fun loadingObserver(): Observer<MatchDetailViewModel.LoadingState> {
        return Observer<MatchDetailViewModel.LoadingState> {
            when (it) {
                MatchDetailViewModel.LoadingState.LOADING -> {
                    binding.progressBar.visibility = VISIBLE
                    binding.loadedInfo.visibility = GONE
                    binding.noticeText.visibility = GONE
                    for (i in 0 until binding.tabMatch.tabCount) {
                        binding.tabMatch.getTabAt(i)!!.view.isClickable = false
                    }
                }
                MatchDetailViewModel.LoadingState.OK -> {
                    binding.progressBar.visibility = GONE
                    binding.loadedInfo.visibility = VISIBLE
                    binding.noticeText.visibility = GONE
                    for (i in 0 until binding.tabMatch.tabCount) {
                        binding.tabMatch.getTabAt(i)!!.view.isClickable = true
                    }
                }
                MatchDetailViewModel.LoadingState.ERROR -> {
                    binding.progressBar.visibility = GONE
                    binding.loadedInfo.visibility = GONE
                    binding.noticeText.visibility = VISIBLE
                    for (i in 0 until binding.tabMatch.tabCount) {
                        binding.tabMatch.getTabAt(i)!!.view.isClickable = false
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if (viewModel.getTabPosition().value!! > 0) {
            viewModel.setTabPosition(viewModel.getTabPosition().value!! - 1)
        } else super.onBackPressed()
    }
}