package com.example.runeterramatchviewer.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.databinding.ActivityMatchesHistoryBinding
import com.example.runeterramatchviewer.di.RuneterraMatchViewerComponent
import com.example.runeterramatchviewer.utils.AssetsMatcher

class MatchesHistoryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMatchesHistoryBinding
    private lateinit var viewModel: MatchesHistoryViewModel
    private val viewModelFactory = RuneterraMatchViewerComponent().viewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMatchesHistoryBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSpinnerAdapter()

        viewModel = ViewModelProvider(this, viewModelFactory)
                .get(MatchesHistoryViewModel::class.java)
        val id = intent.getStringExtra(EXTRA_PLAYER_ID)
        if (savedInstanceState == null) {
            this.viewModel.startInitViewModel(id!!)
        }
        this.viewModel.getNotice().observe(this, noticeObserver())
        this.viewModel.getInfosToDisplay().observe(this, infosToDisplayObserver())
        this.viewModel.getLoading().observe(this, loadingObserver())

        val spinnerListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                val spinner = parent as Spinner
                when (spinner.id) {
                    binding.modeChoiceSpinner.id ->
                        viewModel.applyModeChoice(binding.modeChoiceSpinner.selectedItem.toString())
                    binding.typeChoiceSpinner.id ->
                        viewModel.applyTypeChoice(binding.typeChoiceSpinner.selectedItem.toString())
                    binding.outcomeChoiceSpinner.id ->
                        viewModel.applyOutcomeChoice(binding.outcomeChoiceSpinner.selectedItem.toString())
                    else -> Log.w(
                            "MatchesHistoryActivity",
                            "Spinner Listener not implemented for spinner " + spinner.id
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        this.binding.modeChoiceSpinner.onItemSelectedListener = spinnerListener
        this.binding.typeChoiceSpinner.onItemSelectedListener = spinnerListener
        this.binding.outcomeChoiceSpinner.onItemSelectedListener = spinnerListener
    }

    private fun setSpinnerAdapter() {
        val outcomeValues = listOf(R.string.all, R.string.win, R.string.loss)
                .map { resources.getString(it) }
        binding.outcomeChoiceSpinner.adapter = ArrayAdapter(
                this, android.R.layout.simple_spinner_item, outcomeValues)

        val modeValues = listOf(R.string.all, R.string.constructed, R.string.expedition, R.string.gauntlet)
                .map { resources.getString(it) }
        binding.modeChoiceSpinner.adapter = ArrayAdapter(
                this, android.R.layout.simple_spinner_item, modeValues)

        val typeValues = listOf(R.string.all, R.string.normal, R.string.ranked)
                .map { resources.getString(it) }
        binding.typeChoiceSpinner.adapter = ArrayAdapter(
                this, android.R.layout.simple_spinner_item, typeValues)
    }

    private fun loadingObserver(): Observer<MatchesHistoryViewModel.LoadingState> {
        return Observer<MatchesHistoryViewModel.LoadingState> {
            when (it) {
                MatchesHistoryViewModel.LoadingState.LOADING -> {
                    binding.progressBar.visibility = VISIBLE
                    binding.matchesHistoryRecyclerView.visibility = GONE
                    binding.noticeText.visibility = GONE
                }
                MatchesHistoryViewModel.LoadingState.OK -> {
                    binding.progressBar.visibility = GONE
                    binding.matchesHistoryRecyclerView.visibility = VISIBLE
                    binding.noticeText.visibility = GONE
                }
                MatchesHistoryViewModel.LoadingState.ERROR -> {
                    binding.progressBar.visibility = GONE
                    binding.matchesHistoryRecyclerView.visibility = GONE
                    binding.noticeText.visibility = VISIBLE
                }
            }
        }
    }

    private fun noticeObserver(): Observer<String> {
        return Observer<String> {
            binding.noticeText.text = it
        }
    }

    private fun infosToDisplayObserver(): Observer<MatchesHistoryViewModel.MatchesDetailsAndPlayersName> {
        return Observer<MatchesHistoryViewModel.MatchesDetailsAndPlayersName> { datas ->
            val filterDatas = datas.copy(matchesDetails = datas.matchesDetails.filter { it.idMatch !in datas.hiddenMatchesId })
            binding.matchesNumber.text = getString(R.string.matches_number, filterDatas.matchesDetails.size)
            val myRecyclerAdapter = MatchesHistoryRecyclerAdapter(
                    filterDatas, AssetsMatcher(application.assets), this, resources
            )
            binding.matchesHistoryRecyclerView.layoutManager = LinearLayoutManager(this)
            binding.matchesHistoryRecyclerView.adapter = myRecyclerAdapter
        }
    }
}