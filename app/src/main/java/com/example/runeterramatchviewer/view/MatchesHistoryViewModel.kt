package com.example.runeterramatchviewer.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.interactor.MatchesHistoryInteractor
import com.example.runeterramatchviewer.model.MatchDetail
import com.example.runeterramatchviewer.model.ModeName
import com.example.runeterramatchviewer.model.TypeMatchName
import com.example.runeterramatchviewer.utils.ResourcesUtils
import com.example.runeterramatchviewer.view.MatchesHistoryViewModel.OutcomeName.Companion.fromDisplayName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MatchesHistoryViewModel(
        private val matchesHistoryInteractor: MatchesHistoryInteractor,
        private val resourcesUtils: ResourcesUtils
) : ViewModel() {

    data class MatchesDetailsAndPlayersName(
            val matchesDetails: List<MatchDetail> = emptyList(),
            val hiddenMatchesId: List<String> = emptyList(),
            val playersName: Map<String, String> = emptyMap(),
            val currentPlayerId: String
    )

    private val infosToDisplay = MutableLiveData<MatchesDetailsAndPlayersName>()
    private val notice = MutableLiveData<String>()
    private var loading = MutableLiveData<LoadingState>()

    private var modeFilter = MutableLiveData(ModeName.ALL)
    private var typeFilter = MutableLiveData(TypeMatchName.ALL)
    private var outcomeFilter = MutableLiveData(OutcomeName.ALL)

    fun startInitViewModel(arg: String) {
        viewModelScope.launch { initViewModel(arg) }
    }

    suspend fun initViewModel(arg: String) {
        displayMatchesInformation(arg)
    }

    private suspend fun displayMatchesInformation(playerId: String) {
        loading.value = LoadingState.LOADING
        val result = withContext(Dispatchers.IO) {
            matchesHistoryInteractor.getMatches(playerId)
        }
        when (result) {
            is Either.Left -> {
                notice.value = resourcesUtils.getString(R.string.network_error)
                loading.value = LoadingState.ERROR
            }
            is Either.Right -> {
                displayMatchesDetails(result.b, playerId)
                loading.value = LoadingState.OK
            }
        }
    }

    private suspend fun displayMatchesDetails(
            matchesDetail: List<MatchDetail>,
            playerId: String
    ) {
        val playersId = matchesDetail.flatMap { listOf(it.winner.id, it.loser.id) }.toSet()
        val result = withContext(Dispatchers.IO) {
            val playersIdToName = playersId.associateBy(
                    { it }, {
                matchesHistoryInteractor.getPlayerName(it).orNull()?.name ?: "Player"
            }
            )
            MatchesDetailsAndPlayersName(matchesDetail, emptyList(), playersIdToName, playerId)
        }
        infosToDisplay.value = result
        modeFilter.value = ModeName.ALL
        typeFilter.value = TypeMatchName.ALL
        outcomeFilter.value = OutcomeName.ALL
    }

    fun applyModeChoice(mode: String) {
        this.modeFilter.value = ModeName.fromDisplayName(mode, resourcesUtils)
        applyFilters()
    }

    fun applyTypeChoice(type: String) {
        this.typeFilter.value = TypeMatchName.fromDisplayName(type, resourcesUtils)
        applyFilters()
    }

    fun applyOutcomeChoice(outcome: String) {
        this.outcomeFilter.value = fromDisplayName(outcome, resourcesUtils)
        applyFilters()
    }

    private fun applyFilters() {
        val matches = this.infosToDisplay.value?.matchesDetails ?: emptyList()
        val playerId = this.infosToDisplay.value?.currentPlayerId ?: ""
        val displayedMatches: List<MatchDetail> = matches
                .filter { typeFilter.value == TypeMatchName.ALL || it.gameType == typeFilter.value }
                .filter { modeFilter.value == ModeName.ALL || it.gameMode == modeFilter.value }
                .filter {
                    when (outcomeFilter.value ?: OutcomeName.ALL) {
                        OutcomeName.ALL -> true
                        OutcomeName.WIN -> it.winner.id == playerId
                        OutcomeName.LOSS -> it.loser.id == playerId
                    }
                }
        val hiddenMatchesId = matches.subtract(displayedMatches).map { it.idMatch }
        if (this.infosToDisplay.value != null) {
            this.infosToDisplay.value = this.infosToDisplay.value?.copy(hiddenMatchesId = hiddenMatchesId)
        }
    }

    fun getInfosToDisplay(): LiveData<MatchesDetailsAndPlayersName> {
        return infosToDisplay
    }

    fun getNotice(): LiveData<String> {
        return notice
    }

    fun getLoading(): LiveData<LoadingState> {
        return loading
    }


    enum class OutcomeName {
        ALL, WIN, LOSS;

        companion object {
            fun fromDisplayName(displayName: String, utils: ResourcesUtils): OutcomeName {
                return when (displayName) {
                    utils.getString(R.string.win) -> WIN
                    utils.getString(R.string.loss) -> LOSS
                    else -> ALL
                }
            }
        }
    }

    enum class LoadingState {
        OK, ERROR, LOADING
    }
}