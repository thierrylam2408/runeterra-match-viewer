package com.example.runeterramatchviewer.view

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.model.ModeName
import com.example.runeterramatchviewer.model.Outcome
import com.example.runeterramatchviewer.model.PlayerDetail
import com.example.runeterramatchviewer.utils.AssetsMatcher
import com.example.runeterramatchviewer.utils.DisplayFormat
import com.example.runeterramatchviewer.view.MatchesHistoryViewModel.MatchesDetailsAndPlayersName
import com.google.android.material.snackbar.Snackbar


const val EXTRA_MATCH_ID = "EXTRA_MATCH_ID"

class MatchesHistoryRecyclerAdapter(
        private val matchesHistoryDatas: MatchesDetailsAndPlayersName,
        private val assetsMatcher: AssetsMatcher,
        private val context: Context,
        private val resource: Resources
) : RecyclerView.Adapter<MatchesHistoryRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_listview_matches_history, parent, false)
        return ViewHolder(view, context, resource)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(matchesHistoryDatas, assetsMatcher, position)
    }

    override fun getItemCount(): Int {
        return matchesHistoryDatas.matchesDetails.size
    }

    class ViewHolder(view: View, private val context: Context, private val resource: Resources) :
            RecyclerView.ViewHolder(view), View.OnClickListener {
        private var modeMatch: ModeName? = null
        private var idMatch: String? = null
        private var currentPlayerId: String? = null
        private var cardview: LinearLayout? = null
        private var player1Name: TextView? = null
        private var player2Name: TextView? = null
        private var mode: TextView? = null
        private var type: TextView? = null
        private var colorOutcomeP1: FrameLayout? = null
        private var colorOutcomeP2: FrameLayout? = null
        private var factionsP1: LinearLayout? = null
        private var factionsP2: LinearLayout? = null

        init {
            itemView.setOnClickListener(this)
            cardview = itemView.findViewById(R.id.cardview)
            player1Name = itemView.findViewById(R.id.player1_name_text_view)
            player2Name = itemView.findViewById(R.id.player2_name_text_view)
            mode = itemView.findViewById(R.id.game_mode)
            type = itemView.findViewById(R.id.game_type)
            colorOutcomeP1 = itemView.findViewById(R.id.colorOutcomeP1)
            colorOutcomeP2 = itemView.findViewById(R.id.colorOutcomeP2)
            factionsP1 = itemView.findViewById(R.id.player1_factions)
            factionsP2 = itemView.findViewById(R.id.player2_factions)
        }

        fun bind(
                matchesHistoryDatas: MatchesDetailsAndPlayersName,
                assetsMatcher: AssetsMatcher,
                position: Int
        ) {
            val players = matchesHistoryDatas.playersName
            val matchHistoryData = matchesHistoryDatas.matchesDetails[position]
            idMatch = matchHistoryData.idMatch
            modeMatch = matchHistoryData.gameMode
            currentPlayerId = matchesHistoryDatas.currentPlayerId
            val idBackgroundColor =
                    if (matchHistoryData.getPlayerOutcome(matchesHistoryDatas.currentPlayerId) == Outcome.LOSER) {
                        R.color.card_loss_background
                    } else {
                        R.color.card_win_background
                    }
            val currentPlayer = matchHistoryData.getPlayerDetail(matchesHistoryDatas.currentPlayerId)
            val otherPlayer = matchHistoryData.getOtherPlayerDetail(matchesHistoryDatas.currentPlayerId)

            colorOutcomeP1?.setBackgroundResource(idBackgroundColor)
            colorOutcomeP2?.setBackgroundResource(idBackgroundColor)
            player1Name?.text = players[currentPlayer.id]
            player2Name?.text = players[otherPlayer.id]

            inflateFactions(currentPlayer, factionsP1, assetsMatcher)
            inflateFactions(otherPlayer, factionsP2, assetsMatcher)

            mode?.text = DisplayFormat.capitalize(matchHistoryData.gameMode.name)
            type?.text = DisplayFormat.capitalize(matchHistoryData.gameType.name)


        }

        private fun inflateFactions(player: PlayerDetail, view: LinearLayout?, assetsMatcher: AssetsMatcher){
            view?.weightSum = player.factions.size.toFloat()
            for(faction in player.factions){
                val factionPicture = assetsMatcher.loadRegionIcon(faction)
                val imageView = ImageView(context)
                val param = view?.layoutParams as LinearLayout.LayoutParams?
                param?.weight = 1f
                imageView.layoutParams = param
                imageView.setImageBitmap(factionPicture)
                view?.addView(imageView)
            }
        }

        override fun onClick(v: View?) {
            if (modeMatch != ModeName.GAUNTLET) {
                val intent = Intent(context, MatchDetailActivity::class.java).apply {
                    putExtra(EXTRA_MATCH_ID, idMatch)
                    putExtra(EXTRA_PLAYER_ID, currentPlayerId)
                }
                context.startActivity(intent)
            } else {
                Snackbar.make(v!!,
                        resource.getString(R.string.no_preview_gauntlet),
                        Snackbar.LENGTH_SHORT).show()
            }
        }

    }
}
