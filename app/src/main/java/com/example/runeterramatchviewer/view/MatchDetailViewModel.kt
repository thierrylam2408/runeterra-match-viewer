package com.example.runeterramatchviewer.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.interactor.MatchesHistoryInteractor
import com.example.runeterramatchviewer.model.Deck
import com.example.runeterramatchviewer.model.MatchDetail
import com.example.runeterramatchviewer.utils.ResourcesUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MatchDetailViewModel(
        private val matchesHistoryInteractor: MatchesHistoryInteractor,
        private val resourcesUtils: ResourcesUtils
) : ViewModel() {

    private val notice = MutableLiveData<String>()
    private val loading = MutableLiveData<LoadingState>()
    private val matchDetailsAndPlayersName = MutableLiveData<MatchDetailsAndPlayersName>()
    private val tabPosition = MutableLiveData(0)
    private val favorite = MutableLiveData(false)

    //first: panels expanded for player1, second: panels expanded for player2
    //size depends on the number of factions for each players
    private lateinit var expands: Pair<List<MutableLiveData<Boolean>>, List<MutableLiveData<Boolean>>>

    data class MatchDetailsAndPlayersName(
            val matchDetails: MatchDetail,
            val playersName: Map<String, String> = emptyMap()
    )

    fun startInitViewModel(matchId: String, currentPlayerId: String) {
        viewModelScope.launch { initViewModel(matchId, currentPlayerId) }
    }

    suspend fun initViewModel(matchId: String, currentPlayerId: String) {
        loading.value = LoadingState.LOADING
        val result = withContext(Dispatchers.IO) {
            matchesHistoryInteractor.getMatchById(matchId)
        }
        when (result) {
            is Either.Left -> {
                notice.value = resourcesUtils.getString(R.string.network_error)
                loading.value = LoadingState.ERROR
            }
            is Either.Right -> {
                displayMatchDetail(result.b, currentPlayerId)
                loading.value = LoadingState.OK
            }
        }
    }

    private suspend fun displayMatchDetail(matchDetail: MatchDetail, currentPlayerId: String) {
        val playersId = listOf(matchDetail.winner.id, matchDetail.loser.id)
        val result = withContext(Dispatchers.IO) {
            val playersIdToName = playersId.associateBy(
                    { it }, {
                matchesHistoryInteractor.getPlayerName(it).orNull()?.name ?: "Player"
            }
            )
            MatchDetailsAndPlayersName(
                    matchDetail,
                    playersIdToName
            )
        }
        initExpandsValues(result, currentPlayerId)
        matchDetailsAndPlayersName.value = result


    }

    private fun initExpandsValues(result: MatchDetailsAndPlayersName, currentPlayerId: String) {
        val numbersOfFactionP1: Int
        val numbersOfFactionP2: Int
        if (result.matchDetails.winner.id == currentPlayerId) {
            numbersOfFactionP1 = result.matchDetails.winner.factions.size
            numbersOfFactionP2 = result.matchDetails.loser.factions.size
        } else {
            numbersOfFactionP1 = result.matchDetails.loser.factions.size
            numbersOfFactionP2 = result.matchDetails.winner.factions.size
        }
        expands = Pair(List(numbersOfFactionP1) { MutableLiveData(true) },
                List(numbersOfFactionP2) { MutableLiveData(true) })
    }

    fun getDeck(code: String): Deck {
        return matchesHistoryInteractor.getDeck(code)
    }

    fun getLoading(): LiveData<LoadingState> {
        return loading
    }

    fun getNotice(): LiveData<String> {
        return notice
    }

    fun getMatchDetails(): LiveData<MatchDetailsAndPlayersName> {
        return matchDetailsAndPlayersName
    }

    fun setTabPosition(position: Int) {
        this.tabPosition.value = position
    }

    fun getFavorite(): LiveData<Boolean> {
        return favorite
    }

    fun setFavorite(boolean: Boolean) {
        this.favorite.value = boolean
    }

    fun getTabPosition(): LiveData<Int> {
        return tabPosition
    }

    fun getExpandBoolean(player: Int, position: Int): LiveData<Boolean>? {
        if (player != 0 && player != 1) {
            return null
        }
        return if (player == 0) {
            expands.first.getOrNull(position)
        } else {
            expands.second.getOrNull(position)
        }
    }

    fun toggleExpandBoolean(player: Int, position: Int) {
        if (player != 0 && player != 1) {
            return
        }
        val panelsPlayer = if (player == 0) {
            expands.first
        } else {
            expands.second
        }
        if (position in panelsPlayer.indices) {
            panelsPlayer[position].value = !panelsPlayer[position].value!!
        }
    }

    fun getNumberPanels(player: Int): Int {
        return if (player == 0) {
            expands.first.size
        } else if (player == 1) {
            expands.second.size
        } else {
            0
        }
    }


    enum class LoadingState {
        OK, ERROR, LOADING
    }
}
