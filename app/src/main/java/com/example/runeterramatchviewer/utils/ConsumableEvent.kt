package com.example.runeterramatchviewer.utils

data class ConsumableEvent(val message: String, val consume: Boolean = false)