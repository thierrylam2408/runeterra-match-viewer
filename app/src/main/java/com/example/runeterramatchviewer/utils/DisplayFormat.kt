package com.example.runeterramatchviewer.utils

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

class DisplayFormat {
    companion object {
        fun capitalize(text: String): String {
            return text.substring(0, 1).toUpperCase(Locale.getDefault()) +
                    text.substring(1).toLowerCase(Locale.getDefault())
        }

        fun dateFormat(date: String): String {
            val parser = DateTimeFormatter.ISO_DATE_TIME
            val parseDate = LocalDateTime.parse(date, parser)
            return parseDate.format(DateTimeFormatter.ofPattern("hh:mm dd/MM/yyyy")
                    .withZone(ZoneId.systemDefault()))
        }
    }
}