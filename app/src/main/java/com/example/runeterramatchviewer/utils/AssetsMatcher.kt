package com.example.runeterramatchviewer.utils

import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.runeterramatchviewer.model.RegionName
import com.example.runeterramatchviewer.model.RegionName.*
import java.io.File
import java.io.IOException
import java.io.InputStream

class AssetsMatcher(private val assetManager: AssetManager) {

    private val root = "img" + File.separator + "regions" + File.separator

    fun loadRegionIcon(region: RegionName?): Bitmap {
        var path = root + "icon-"
        path += matchRegion(region)
        path += ".png"
        return bitmapFactory(path)
    }

    private fun matchRegion(region: RegionName?): String {
        return when (region) {
            BILGEWATER -> "bilgewater"
            DEMACIA -> "demacia"
            FRELJORD -> "freljord"
            IONIA -> "ionia"
            NOXUS -> "noxus"
            PILTOVERZAUN -> "piltoverzaun"
            SHADOWISLES -> "shadowisles"
            TARGON -> "targon"
            else -> "all"
        }
    }

    private fun bitmapFactory(path: String): Bitmap {
        var istr: InputStream? = null
        try {
            istr = assetManager.open(path)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return BitmapFactory.decodeStream(istr)
    }


}
