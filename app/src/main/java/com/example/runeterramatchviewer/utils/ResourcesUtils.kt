package com.example.runeterramatchviewer.utils

import android.content.Context


class ResourcesUtils(private val appContext: Context) {
    fun getString(resId: Int): String {
        return appContext.getString(resId)
    }

    fun getString(resId: Int, vararg formatArgs: String): String {
        return appContext.getString(resId, *formatArgs)
    }

    fun getFile(path: String): String {
        return appContext.assets.open(path).bufferedReader().use { it.readText() }
    }

}