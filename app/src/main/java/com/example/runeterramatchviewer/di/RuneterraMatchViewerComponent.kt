package com.example.runeterramatchviewer.di

import com.example.runeterramatchviewer.interactor.MatchesHistoryInteractor
import com.example.runeterramatchviewer.interactor.PlayerIdentificationInteractor
import com.example.runeterramatchviewer.model.MatchDetailsLocalRepository
import com.example.runeterramatchviewer.model.MatchDetailsRemoteRepository
import com.example.runeterramatchviewer.model.MatchesIdRepository
import com.example.runeterramatchviewer.model.PlayerIdRepository
import com.example.runeterramatchviewer.repository.*
import com.example.runeterramatchviewer.utils.ResourcesUtils
import com.example.runeterramatchviewer.view.MatchDetailViewModel
import com.example.runeterramatchviewer.view.MatchesHistoryViewModel
import com.example.runeterramatchviewer.view.PlayerIdViewModel
import com.example.runeterramatchviewer.view.ViewModelFactory
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.dsl.module

class RuneterraMatchViewerComponent() : KoinComponent {

    val viewModelFactory by inject<ViewModelFactory>()

    val appModule = module {
        single { ViewModelFactory(get(), get(), get()) }
        single { ResourcesUtils(get()) }
    }

    val networkModule = module {
        single { RetrofitWrapper(context = get()) }
        single { NetworkManager(get()) }
    }
    val playerIdentificationModule = module {
        single<PlayerIdRepository> { PlayerIdRemoteRepository(get(), get()) }
        single { PlayerIdentificationInteractor(get()) }
        single { PlayerIdViewModel(get(), get()) }
    }

    val matchesHistoryModule = module {
        single<MatchesIdRepository> { MatchesRemoteIdRepository(get()) }
        single<MatchDetailsRemoteRepository> { MatchDetailsWebRepository(get()) }
        single<MatchDetailsLocalRepository> { MatchDetailsRoomRepository() }
        single { MatchesHistoryInteractor(get(), get(), get(), get()) }
        single { MatchesHistoryViewModel(get(), get()) }
        single { DeckDecoderJsonReaderRepository(get()) }
    }

    val matchDetailModule = module {
        single { MatchDetailViewModel(get(), get()) }
    }


}