package com.example.runeterramatchviewer.repository

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.example.runeterramatchviewer.model.AppError
import com.example.runeterramatchviewer.model.MatchesIdRepository
import com.example.runeterramatchviewer.model.SUCCESS_CODE
import com.example.runeterramatchviewer.model.UnprocessedNetworkError
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

class MatchesRemoteIdRepository(
        retrofit: RetrofitWrapper
) : MatchesIdRepository {

    private var matchesIDService: MatchesHistoryIDService =
            retrofit.get().create(MatchesHistoryIDService::class.java)

    override fun getMatchesId(playerId: String): Either<AppError, List<String>> {
        val call = matchesIDService.matchesId(playerId)!!.execute()
        return when (call.code()) {
            SUCCESS_CODE -> Right(call.body()!!)
            else -> Left(UnprocessedNetworkError)
        }
    }
}

interface MatchesHistoryIDService {
    @GET("lor/match/v1/matches/by-puuid/{puuid}/ids")
    fun matchesId(@Path("puuid") playerId: String): Call<List<String>>?
}