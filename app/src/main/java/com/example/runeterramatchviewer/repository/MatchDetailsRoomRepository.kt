package com.example.runeterramatchviewer.repository

import arrow.core.Either
import arrow.core.Right
import com.example.runeterramatchviewer.model.AppError
import com.example.runeterramatchviewer.model.MatchDetail
import com.example.runeterramatchviewer.model.MatchDetailsLocalRepository


class MatchDetailsRoomRepository :
        MatchDetailsLocalRepository {
    override fun isPresent(matchId: String): Boolean {
        return false
    }

    override fun persist(match: MatchDetail) {
        return
    }

    override fun getMatchDetail(matchId: String): MatchDetail {
        TODO("Not yet implemented")
    }

    override fun getMatchDetailOrNull(matchId: String): Either<AppError, MatchDetail?> {
        return Right(null)
    }


}