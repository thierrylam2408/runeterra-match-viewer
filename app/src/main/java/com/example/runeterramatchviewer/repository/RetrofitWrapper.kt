package com.example.runeterramatchviewer.repository

import android.content.Context
import com.example.runeterramatchviewer.BuildConfig
import com.google.common.util.concurrent.RateLimiter
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

class RetrofitWrapper(url: String = "https://europe.api.riotgames.com/", context: Context) {

    companion object {
        const val CALLS_PER_SECOND = 15.0
        const val CACHE_SIZE: Long = 50L * 1024L * 1024L
        const val MAX_AGE_CACHE = 120 //value in seconds
    }

    private val retrofit: Retrofit

    init {

        val cache = Cache(directory = File(context.cacheDir, "http_cache"), maxSize = CACHE_SIZE)
        cache.initialize()
        val client = OkHttpClient.Builder()
                .addInterceptor(CustomInterceptors.getHeadersInterceptor())
                .addInterceptor(CustomInterceptors.getRateLimitInterceptor())
                .addInterceptor { chain ->
                    val xRiotToken = BuildConfig.X_RIOT_TOKEN_API_KEY
                    val request: Request = chain.request().newBuilder()
                            .addHeader("X-Riot-Token", xRiotToken)
                            .build()
                    chain.proceed(request)
                }
                .addNetworkInterceptor(CustomInterceptors.getCacheControlInterceptor())
                .cache(cache)
                .build()
        retrofit = Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    fun get(): Retrofit {
        return retrofit
    }

    private class CustomInterceptors {
        private class RateLimitInterceptor : Interceptor {
            private val rateLimiter: RateLimiter = RateLimiter.create(CALLS_PER_SECOND)

            override fun intercept(chain: Interceptor.Chain): Response {
                rateLimiter.acquire(1)
                return chain.proceed(chain.request())
            }
        }

        companion object {
            fun getRateLimitInterceptor(): RateLimitInterceptor {
                return RateLimitInterceptor()
            }

            fun getHeadersInterceptor(): HttpLoggingInterceptor {
                return HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.HEADERS
                }
            }

            private class CacheControlInterceptor : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    return chain.proceed(chain.request()).newBuilder()
                            .header("Cache-Control", "public, max-age=$MAX_AGE_CACHE")
                            .build();
                }

            }

            fun getCacheControlInterceptor(): Interceptor {
                return CacheControlInterceptor()
            }
        }
    }
}