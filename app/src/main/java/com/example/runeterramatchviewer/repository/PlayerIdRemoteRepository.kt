package com.example.runeterramatchviewer.repository

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.example.runeterramatchviewer.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

class PlayerIdRemoteRepository(
        retrofit: RetrofitWrapper,
        private val networkManager: NetworkManager
) :
        PlayerIdRepository {

    private var service: PlayerIdService

    init {
        service = retrofit.get().create(PlayerIdService::class.java)
    }

    override fun getPlayerId(playerName: String, tagName: String): Either<AppError, String> {
        return when (networkManager.isNetworkAvailable()) {
            false -> Left(NoNetworkError)
            else -> {
                val call = service.playerId(playerName, tagName)!!.execute()
                when (call.code()) {
                    SUCCESS_CODE -> Right(call.body()!!.puuid)
                    PLAYER_NOT_FOUND_CODE -> Left(HttpError(PLAYER_NOT_FOUND_CODE))
                    else -> Left(UnprocessedNetworkError)
                }
            }
        }
    }

    interface PlayerIdService {
        @GET("riot/account/v1/accounts/by-riot-id/{playerName}/{tagName}")
        fun playerId(
                @Path("playerName") playerName: String,
                @Path("tagName") tagName: String
        ): Call<PlayerIdData>?
    }

    data class PlayerIdData(val puuid: String, val gameName: String, val tagLine: String)

}
