package com.example.runeterramatchviewer.repository

import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.model.RarityName.*
import com.example.runeterramatchviewer.model.RegionName.*
import com.example.runeterramatchviewer.model.TypeCardName.*
import com.example.runeterramatchviewer.utils.ResourcesUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import no.stelar7.api.r4j.impl.lor.LoRDeckCode
import no.stelar7.api.r4j.pojo.lor.offline.card.LoRDeck
import java.io.File

class DeckDecoderJsonReaderRepository(
        resourcesUtils: ResourcesUtils,
        private val decoder: (String) -> LoRDeck = LoRDeckCode::decode
) : DeckDecoderRepository {

    private val root = "cards${File.separator}"
    private val set1Path = "${root}set1-en_us.json"
    private val set2Path = "${root}set2-en_us.json"
    private val set3Path = "${root}set3-en_us.json"
    private val cardsSet1: List<CardJson>
    private val cardsSet2: List<CardJson>
    private val cardsSet3: List<CardJson>

    private data class CardJson(val cardCode: String,
                                val name: String,
                                val region: String,
                                val type: String,
                                val rarity: String)

    init {
        val cardsType = object : TypeToken<List<CardJson>>() {}.type
        val stringS1 = resourcesUtils.getFile(set1Path)
        cardsSet1 = Gson().fromJson(stringS1, cardsType)
        val stringS2 = resourcesUtils.getFile(set2Path)
        cardsSet2 = Gson().fromJson(stringS2, cardsType)
        val stringS3 = resourcesUtils.getFile(set3Path)
        cardsSet3 = Gson().fromJson(stringS3, cardsType)
    }

    override fun decode(code: String): Deck {
        val decode = decoder(code)
        val cardUnknown = CardJson("??", "??", "??", "??", "??")
        val cards: Map<Card, Int> = decode.deck
                .mapKeys { (lorCard, _) ->
                    if (lorCard.set == 1) {
                        cardsSet1.find { lorCard.cardCode == it.cardCode } ?: cardUnknown
                    } else if (lorCard.set == 2) {
                        cardsSet2.find { lorCard.cardCode == it.cardCode } ?: cardUnknown
                    } else if (lorCard.set == 3) {
                        cardsSet3.find { lorCard.cardCode == it.cardCode } ?: cardUnknown
                    } else {
                        cardUnknown
                    }

                }
                .mapKeys { (cardJson, _) ->
                    Card(cardJson.name,
                            matchRegion(cardJson.region),
                            matchType(cardJson.type),
                            matchRarity(cardJson.rarity))
                }
        return Deck(cards)
    }

    private fun matchRegion(region: String?): RegionName {
        return when (region) {
            "Bilgewater" -> BILGEWATER
            "Demacia" -> DEMACIA
            "Freljord" -> FRELJORD
            "Ionia" -> IONIA
            "Noxus" -> NOXUS
            "Piltover & Zaun" -> PILTOVERZAUN
            "Shadow Isles" -> SHADOWISLES
            "Targon" -> TARGON
            else -> UNKNOWN
        }
    }

    private fun matchType(type: String?): TypeCardName {
        return when (type) {
            "Unit" -> UNIT
            "Spell" -> SPELL
            "Landmark" -> LANDMARK
            else -> TypeCardName.NONE
        }
    }

    private fun matchRarity(rarity: String?): RarityName {
        return when (rarity) {
            "Champion" -> CHAMPION
            "EPIC" -> EPIC
            "RARE" -> RARE
            "COMMON" -> COMMON
            else -> RarityName.NONE
        }
    }
}