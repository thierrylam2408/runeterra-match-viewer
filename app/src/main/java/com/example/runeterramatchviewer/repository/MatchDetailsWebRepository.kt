package com.example.runeterramatchviewer.repository

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.model.RegionName.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.*

class MatchDetailsWebRepository(
        retrofit: RetrofitWrapper
) : MatchDetailsRemoteRepository {

    private var matchesDetailsService: MatchDetailsService =
            retrofit.get().create(MatchDetailsService::class.java)
    private var playerNameService: PlayerNameService =
            retrofit.get().create(PlayerNameService::class.java)

    override fun getMatchDetail(matchId: String): Either<AppError, MatchDetail> {
        val call = matchesDetailsService.matches(matchId)!!.execute()
        return when (call.code()) {
            SUCCESS_CODE -> Right(convert(call.body()!!))
            else -> Left(UnprocessedNetworkError)
        }
    }

    override fun getPlayerName(playerId: String): Either<AppError, PlayerInfo> {
        val call = playerNameService.name(playerId)!!.execute()
        return when (call.code()) {
            200 -> Right(PlayerInfo(playerId, call.body()!!.gameName))
            else -> Left(UnprocessedNetworkError)
        }
    }

    interface MatchDetailsService {
        @GET("lor/match/v1/matches/{matchId}")
        fun matches(@Path("matchId") matchId: String): Call<MatchInfoJson>?
    }

    interface PlayerNameService {
        @GET("riot/account/v1/accounts/by-puuid/{puuid}")
        fun name(@Path("puuid") puuid: String): Call<PlayerNameInfoJson>?
    }

    data class MatchInfoJson(
            val metadata: MetaDataJson,
            val info: InfoJson
    )

    data class MetaDataJson(
            val data_version: Int,
            val match_id: String,
            val participants: List<String>
    )

    data class InfoJson(
            val game_mode: String, val game_type: String, val game_start_time_utc: String,
            val game_version: String, val players: List<PlayerJson>, val total_turn_count: Int
    )

    data class PlayerJson(
            val puuid: String, val deck_id: String, val deck_code: String, val factions: List<String>,
            val game_outcome: String, val order_of_play: String
    )

    data class PlayerNameInfoJson(val puuid: String, val gameName: String, val tagLine: String)

    companion object {

        private fun getPlayerByOutcome(matchInfoJson: MatchInfoJson, outcome: String): PlayerJson {
            val players = matchInfoJson.info.players
            return if (players[0].game_outcome.toLowerCase(Locale.getDefault()) == outcome
                    || players.size == 1) {
                players[0]
            } else {
                players[1]
            }
        }

        fun convert(matchInfoJson: MatchInfoJson): MatchDetail {
            val winnerJson = getPlayerByOutcome(matchInfoJson, "win")
            val winner = PlayerDetail(
                    winnerJson.puuid,
                    winnerJson.factions.map { regionMapping.getOrDefault(it, UNKNOWN) },
                    winnerJson.deck_code)
            val loserJson = getPlayerByOutcome(matchInfoJson, "loss")
            val loser = PlayerDetail(
                    loserJson.puuid,
                    loserJson.factions.map { regionMapping.getOrDefault(it, UNKNOWN) },
                    loserJson.deck_code)
            return MatchDetail(
                    matchInfoJson.metadata.match_id,
                    modeMapping.getOrDefault(matchInfoJson.info.game_mode, ModeName.GAUNTLET),
                    typeMapping.getOrDefault(matchInfoJson.info.game_type, TypeMatchName.NORMAL),
                    winner,
                    loser,
                    matchInfoJson.info.total_turn_count,
                    matchInfoJson.info.game_start_time_utc
            )
        }

        private val regionMapping = mapOf(
                "faction_Bilgewater_Name" to BILGEWATER,
                "faction_Demacia_Name" to DEMACIA,
                "faction_Freljord_Name" to FRELJORD,
                "faction_Ionia_Name" to IONIA,
                "faction_Noxus_Name" to NOXUS,
                "faction_Piltover_Name" to PILTOVERZAUN,
                "faction_ShadowIsles_Name" to SHADOWISLES,
                "faction_MtTargon_Name" to TARGON
        )

        private val modeMapping = mapOf(
                "Constructed" to ModeName.CONSTRUCTED,
                "Expeditions" to ModeName.EXPEDITION
        )

        private val typeMapping = mapOf(
                "Normal" to TypeMatchName.NORMAL,
                "Ranked" to TypeMatchName.RANKED
        )
    }
}