package com.example.runeterramatchviewer.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.core.Either
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.interactor.MatchesHistoryInteractor
import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.utils.ResourcesUtils
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MatchDetailViewModelTest {

    @get:Rule
    val instantTestExecutorRule = InstantTaskExecutorRule()

    private var matchesHistoryInteractor: MatchesHistoryInteractor = mockk()
    private val resourcesUtils: ResourcesUtils = mockk()

    private val idPlayer1 = "idPlayer1"
    private val idPlayer2 = "idPlayer2"
    private val playerInfo1 = PlayerInfo(idPlayer1, "Player 1")
    private val playerInfo2 = PlayerInfo(idPlayer2, "Player 2")
    private val idMatch1 = "idmatch1"
    private val player1 = PlayerDetail(
            idPlayer1,
            listOf(RegionName.SHADOWISLES, RegionName.PILTOVERZAUN),
            "0000"
    )
    private val player2 = PlayerDetail(
            idPlayer2,
            listOf(RegionName.IONIA, RegionName.FRELJORD),
            "0001"
    )
    private val playersName = mutableMapOf(
            idPlayer1 to playerInfo1.name,
            idPlayer2 to playerInfo2.name
    )
    private val match = MatchDetail(
            idMatch1,
            ModeName.CONSTRUCTED,
            TypeMatchName.NORMAL,
            player1,
            player2,
            15,
            "2019-12-05T15:35:12"
    )

    @Before
    fun setup() {
        every { matchesHistoryInteractor.getPlayerName(idPlayer1) } returns Either.Right(playerInfo1)
        every { matchesHistoryInteractor.getPlayerName(idPlayer2) } returns Either.Right(playerInfo2)
        every { resourcesUtils.getString(R.string.all) } returns "All"
        every { resourcesUtils.getString(R.string.win) } returns "Win"
        every { resourcesUtils.getString(R.string.loss) } returns "Loss"
        every { resourcesUtils.getString(R.string.constructed) } returns "Constructed"
        every { resourcesUtils.getString(R.string.expedition) } returns "Expedition"
        every { resourcesUtils.getString(R.string.gauntlet) } returns "Gauntlet"
        every { resourcesUtils.getString(R.string.normal) } returns "Normal"
        every { resourcesUtils.getString(R.string.ranked) } returns "Ranked"
    }

    @Test
    fun getMatchesId_should_change_infosToDisplay_when_interactor_return_match() {
        //GIVEN
        every { matchesHistoryInteractor.getMatchById(any()) } returns Either.Right(match)
        val matchDetailViewModel =
                MatchDetailViewModel(matchesHistoryInteractor, resourcesUtils)

        //WHEN
        runBlocking { matchDetailViewModel.initViewModel(idMatch1, idPlayer1) }

        //THEN
        val expected = MatchDetailViewModel.MatchDetailsAndPlayersName(match, playersName)
        assertEquals(
                MatchDetailViewModel.LoadingState.OK,
                matchDetailViewModel.getLoading().value
        )
        assertEquals(expected, matchDetailViewModel.getMatchDetails().value)
    }

    @Test
    fun getMatchesId_should_notify_when_interactor_return_error() {
        //GIVEN
        val errorMessage = "Network failure, try again later.."
        every { matchesHistoryInteractor.getMatchById(any()) } returns
                Either.Left(UnprocessedNetworkError)
        every { resourcesUtils.getString(any()) } returns errorMessage

        val matchDetailViewModel =
                MatchDetailViewModel(matchesHistoryInteractor, resourcesUtils)

        //WHEN
        runBlocking { matchDetailViewModel.initViewModel(idMatch1, idPlayer1) }

        //THEN
        assertEquals(
                MatchDetailViewModel.LoadingState.ERROR,
                matchDetailViewModel.getLoading().value
        )
        assertEquals(null, matchDetailViewModel.getMatchDetails().value)
        assertEquals(
                errorMessage, matchDetailViewModel.getNotice().value
        )
    }
}