package com.example.runeterramatchviewer.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.core.Left
import arrow.core.Right
import com.example.runeterramatchviewer.interactor.PlayerIdentificationInteractor
import com.example.runeterramatchviewer.model.HttpError
import com.example.runeterramatchviewer.model.InputError
import com.example.runeterramatchviewer.model.PLAYER_NOT_FOUND_CODE
import com.example.runeterramatchviewer.model.UnprocessedNetworkError
import com.example.runeterramatchviewer.utils.ConsumableEvent
import com.example.runeterramatchviewer.utils.ResourcesUtils
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class PlayerIdViewModelTest {

    @get:Rule
    val instantTestExecutorRule = InstantTaskExecutorRule()

    private var playerIdentificationInteractor: PlayerIdentificationInteractor = mockk()
    private val resourcesUtils: ResourcesUtils = mockk()

    private val anyPlayerName: String = "NAME_TEST"
    private val anyTagName: String = "TAG_TEST"
    private val goodPlayerId: String = "GOOD_PLAYER_ID"
    private val wrongPlayerName: String = "WRONG_PLAYER_NAME"
    private val wrongPlayerTag: String = "WRONG_PLAYER_TAG"

    @Test
    fun getPlayerId_should_change_id_live_data_when_entries_are_right() {
        //GIVEN
        every { playerIdentificationInteractor.getPlayerId(any(), any()) } returns Right(
                goodPlayerId
        )
        val playerIdentificationViewModel = PlayerIdViewModel(
                playerIdentificationInteractor, resourcesUtils)
        playerIdentificationViewModel.editPlayerName(anyPlayerName)
        playerIdentificationViewModel.editTagName(anyTagName)

        //WHEN
        runBlocking { playerIdentificationViewModel.getPlayerId() }

        //THEN
        assertEquals(
                ConsumableEvent(goodPlayerId),
                playerIdentificationViewModel.getId().value
        )
    }

    @Test
    fun getPlayerId_should_notify_PlayerNotFound_error_when_interactor_didnt_find_id() {
        //GIVEN
        val errorMessage = "$wrongPlayerName in server $wrongPlayerTag not found"
        val playerIdentificationViewModel = PlayerIdViewModel(
                playerIdentificationInteractor, resourcesUtils)
        playerIdentificationViewModel.editPlayerName(wrongPlayerName)
        playerIdentificationViewModel.editTagName(wrongPlayerTag)
        every { playerIdentificationInteractor.getPlayerId(any(), any()) } returns
                Left(HttpError(PLAYER_NOT_FOUND_CODE))
        every { resourcesUtils.getString(any(), any(), any()) } returns errorMessage


        //WHEN
        runBlocking { playerIdentificationViewModel.getPlayerId() }

        //THEN
        assertEquals(ConsumableEvent(errorMessage),
                playerIdentificationViewModel.getNotice().value
        )
    }

    @Test
    fun getPlayerId_should_notify_InputError_error_when_interactor_get_input_error() {
        //GIVEN
        val errorMessage = "Wrong Input"
        every { playerIdentificationInteractor.getPlayerId(any(), any()) } returns Left(InputError)
        every { resourcesUtils.getString(any()) } returns errorMessage
        val playerIdentificationViewModel = PlayerIdViewModel(
                playerIdentificationInteractor, resourcesUtils
        )

        //WHEN
        runBlocking { playerIdentificationViewModel.getPlayerId() }

        //THEN
        assertEquals(
                ConsumableEvent(errorMessage),
                playerIdentificationViewModel.getNotice().value
        )
    }

    @Test
    fun getPlayerId_should_notify_Network_error_when_interactor_get_network_error() {
        //GIVEN
        val errorMessage = "Network Error"
        val playerIdentificationViewModel = PlayerIdViewModel(
                playerIdentificationInteractor, resourcesUtils
        )
        playerIdentificationViewModel.editPlayerName(anyPlayerName)
        playerIdentificationViewModel.editTagName(anyTagName)
        every { playerIdentificationInteractor.getPlayerId(any(), any()) } returns Left(
                UnprocessedNetworkError
        )
        every { resourcesUtils.getString(any()) } returns errorMessage

        //WHEN
        runBlocking { playerIdentificationViewModel.getPlayerId() }

        //THEN
        assertEquals(
                ConsumableEvent(errorMessage),
                playerIdentificationViewModel.getNotice().value
        )
    }
}