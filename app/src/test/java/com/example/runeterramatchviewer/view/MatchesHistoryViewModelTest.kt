package com.example.runeterramatchviewer.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.core.Either
import com.example.runeterramatchviewer.R
import com.example.runeterramatchviewer.interactor.MatchesHistoryInteractor
import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.utils.ResourcesUtils
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MatchesHistoryViewModelTest {

    @get:Rule
    val instantTestExecutorRule = InstantTaskExecutorRule()

    private var matchesHistoryInteractor: MatchesHistoryInteractor = mockk()
    private val resourcesUtils: ResourcesUtils = mockk()

    private val idPlayer1 = "idPlayer1"
    private val idPlayer2 = "idPlayer2"
    private val idPlayer3 = "idPlayer3"
    private val playerInfo1 = PlayerInfo(idPlayer1, "Player 1")
    private val playerInfo2 = PlayerInfo(idPlayer2, "Player 2")
    private val playerInfo3 = PlayerInfo(idPlayer3, "Player 3")
    private val playersName = mutableMapOf(
            idPlayer1 to playerInfo1.name,
            idPlayer2 to playerInfo2.name,
            idPlayer3 to playerInfo3.name
    )
    private val idMatch1 = "idmatch1"
    private val idMatch2 = "idmatch2"
    private val idMatch3 = "idmatch3"
    private val player1 = PlayerDetail(
            idPlayer1,
            listOf(RegionName.SHADOWISLES, RegionName.PILTOVERZAUN),
            "0000"
    )
    private val player2 = PlayerDetail(
            idPlayer2,
            listOf(RegionName.IONIA, RegionName.FRELJORD),
            "0001"
    )
    private val player3 = PlayerDetail(
            idPlayer3,
            listOf(RegionName.DEMACIA, RegionName.TARGON),
            "0010"
    )
    private val m1 = MatchDetail(
            idMatch1,
            ModeName.CONSTRUCTED,
            TypeMatchName.NORMAL,
            player1,
            player2,
            15,
            "2019-12-05T15:35:12"
    )
    private val m2 = MatchDetail(
            idMatch2,
            ModeName.EXPEDITION,
            TypeMatchName.RANKED,
            player3,
            player1,
            17,
            "2019-12-06T20:32:21"
    )
    private val m3 = MatchDetail(
            idMatch3,
            ModeName.CONSTRUCTED,
            TypeMatchName.RANKED,
            player1,
            player3,
            26,
            "2019-12-05T17:01:10"
    )
    private val matches = listOf(m1, m2, m3)

    @Before
    fun setup() {
        every { matchesHistoryInteractor.getPlayerName(idPlayer1) } returns Either.Right(playerInfo1)
        every { matchesHistoryInteractor.getPlayerName(idPlayer2) } returns Either.Right(playerInfo2)
        every { matchesHistoryInteractor.getPlayerName(idPlayer3) } returns Either.Right(playerInfo3)
        every { resourcesUtils.getString(R.string.all) } returns "All"
        every { resourcesUtils.getString(R.string.win) } returns "Win"
        every { resourcesUtils.getString(R.string.loss) } returns "Loss"
        every { resourcesUtils.getString(R.string.constructed) } returns "Constructed"
        every { resourcesUtils.getString(R.string.expedition) } returns "Expedition"
        every { resourcesUtils.getString(R.string.gauntlet) } returns "Gauntlet"
        every { resourcesUtils.getString(R.string.normal) } returns "Normal"
        every { resourcesUtils.getString(R.string.ranked) } returns "Ranked"
    }

    @Test
    fun getMatchesId_should_change_infosToDisplay_when_interactor_return_matches() {
        //GIVEN
        every { matchesHistoryInteractor.getMatches(any()) } returns Either.Right(matches)
        val matchesHistoryViewModel =
                MatchesHistoryViewModel(matchesHistoryInteractor, resourcesUtils)

        //WHEN
        runBlocking { matchesHistoryViewModel.initViewModel(idPlayer1) }

        //THEN
        val expected =
                MatchesHistoryViewModel.MatchesDetailsAndPlayersName(
                        matches,
                        emptyList(),
                        playersName,
                        idPlayer1
                )
        assertEquals(
                MatchesHistoryViewModel.LoadingState.OK,
                matchesHistoryViewModel.getLoading().value
        )
        assertEquals(expected, matchesHistoryViewModel.getInfosToDisplay().value)
    }

    @Test
    fun getMatchesId_should_notify_when_interactor_return_error() {
        //GIVEN
        val errorMessage = "Network failure, try again later.."
        every { matchesHistoryInteractor.getMatches(any()) } returns
                Either.Left(UnprocessedNetworkError)
        every { resourcesUtils.getString(any()) } returns errorMessage

        val matchesHistoryViewModel =
                MatchesHistoryViewModel(matchesHistoryInteractor, resourcesUtils)

        //WHEN
        runBlocking { matchesHistoryViewModel.initViewModel(idPlayer1) }

        //THEN
        assertEquals(
                MatchesHistoryViewModel.LoadingState.ERROR,
                matchesHistoryViewModel.getLoading().value
        )
        assertEquals(null, matchesHistoryViewModel.getInfosToDisplay().value)
        assertEquals(
                errorMessage, matchesHistoryViewModel.getNotice().value
        )
    }

    @Test
    fun filter_with_constructed_ranked_match_should_show_only_m3() {
        //GIVEN
        every { matchesHistoryInteractor.getMatches(any()) } returns Either.Right(matches)
        val matchesHistoryViewModel =
                MatchesHistoryViewModel(matchesHistoryInteractor, resourcesUtils)

        //WHEN
        runBlocking { matchesHistoryViewModel.initViewModel(idPlayer1) }
        matchesHistoryViewModel.applyModeChoice("Constructed")
        matchesHistoryViewModel.applyTypeChoice("Ranked")

        //THEN
        val expected = MatchesHistoryViewModel.MatchesDetailsAndPlayersName(
                listOf(m1, m2, m3),
                listOf(m1.idMatch, m2.idMatch),
                playersName,
                idPlayer1
        )
        assertEquals(
                MatchesHistoryViewModel.LoadingState.OK,
                matchesHistoryViewModel.getLoading().value
        )
        assertEquals(expected, matchesHistoryViewModel.getInfosToDisplay().value)
    }

    @Test
    fun filter_winner_outcome_match_should_show_only_m1_and_m3() {
        //GIVEN
        every { matchesHistoryInteractor.getMatches(any()) } returns Either.Right(matches)
        val matchesHistoryViewModel =
                MatchesHistoryViewModel(matchesHistoryInteractor, resourcesUtils)

        //WHEN
        runBlocking { matchesHistoryViewModel.initViewModel(idPlayer1) }
        matchesHistoryViewModel.applyOutcomeChoice("Win")

        //THEN
        val expected = MatchesHistoryViewModel.MatchesDetailsAndPlayersName(
                listOf(m1, m2, m3),
                listOf(m2.idMatch),
                playersName,
                idPlayer1
        )
        assertEquals(
                MatchesHistoryViewModel.LoadingState.OK,
                matchesHistoryViewModel.getLoading().value
        )
        assertEquals(expected, matchesHistoryViewModel.getInfosToDisplay().value)
    }
}