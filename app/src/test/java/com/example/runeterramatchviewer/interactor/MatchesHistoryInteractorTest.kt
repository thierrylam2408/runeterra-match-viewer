package com.example.runeterramatchviewer.interactor


import arrow.core.Left
import arrow.core.Right
import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.repository.DeckDecoderJsonReaderRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MatchesHistoryInteractorTest {

    private var matchesIdRepository: MatchesIdRepository = mockk()
    private var matchDetailsRemoteRepository: MatchDetailsRemoteRepository = mockk()
    private var matchDetailsLocalRepository: MatchDetailsLocalRepository = mockk()
    private var deckDecoderRepository: DeckDecoderJsonReaderRepository = mockk()

    private val idPlayer1 = "idPlayer1"
    private val idPlayer2 = "idPlayer2"
    private val idPlayer3 = "idPlayer3"
    private val playerInfo1 = PlayerInfo(idPlayer1, "Player 1")
    private val playerInfo2 = PlayerInfo(idPlayer2, "Player 2")
    private val playerInfo3 = PlayerInfo(idPlayer3, "Player 3")
    private val idMatch1 = "idMatch1"
    private val idMatch2 = "idMatch2"
    private val idMatch3 = "idMatch3"
    private val idsMatch = listOf(idMatch1, idMatch2, idMatch3)
    private val player1 = PlayerDetail(
            idPlayer1,
            listOf(RegionName.SHADOWISLES, RegionName.PILTOVERZAUN),
            "0000"
    )
    private val player2 = PlayerDetail(
            idPlayer2,
            listOf(RegionName.IONIA, RegionName.FRELJORD),
            "0001"
    )
    private val player3 = PlayerDetail(
            idPlayer3,
            listOf(RegionName.DEMACIA, RegionName.TARGON),
            "0010"
    )
    private val m1 = MatchDetail(
            idMatch1,
            ModeName.CONSTRUCTED,
            TypeMatchName.NORMAL,
            player1,
            player2,
            15,
            "2019-12-05T15:35:12"
    )
    private val m2 = MatchDetail(
            idMatch2,
            ModeName.EXPEDITION,
            TypeMatchName.RANKED,
            player3,
            player1,
            17,
            "2019-12-06T20:32:21"
    )
    private val m3 = MatchDetail(
            idMatch3,
            ModeName.CONSTRUCTED,
            TypeMatchName.RANKED,
            player1,
            player3,
            26,
            "2019-12-05T17:01:10"
    )
    private val matches = listOf(m1, m2, m3)

    @Before
    fun setup() {
        every { matchDetailsRemoteRepository.getPlayerName(idPlayer1) } returns Right(playerInfo1)
        every { matchDetailsRemoteRepository.getPlayerName(idPlayer2) } returns Right(playerInfo2)
        every { matchDetailsRemoteRepository.getPlayerName(idPlayer3) } returns Right(playerInfo3)
    }

    @Test
    fun getMatches_should_return_ErrorRequest_when_repository_fails() {
        //GIVEN
        every { matchesIdRepository.getMatchesId(any()) } returns Left(UnprocessedNetworkError)
        val matchesHistoryInteractor = MatchesHistoryInteractor(
                matchesIdRepository,
                matchDetailsRemoteRepository,
                matchDetailsLocalRepository,
                deckDecoderRepository
        )

        //WHEN
        val res = matchesHistoryInteractor.getMatches("0000")

        //THEN
        assertEquals(Left(UnprocessedNetworkError), res)
    }

    @Test
    fun matchDetailsRepository_should_return_matches_when_repository_successfully_find_player_id() {
        //GIVEN
        every { matchesIdRepository.getMatchesId(idPlayer1) } returns Right(idsMatch)
        every { matchDetailsLocalRepository.getMatchDetailOrNull(any()) } returns Right(null)
        every { matchDetailsLocalRepository.persist(any()) } returns Unit
        every { matchDetailsRemoteRepository.getMatchDetail(idMatch1) } returns Right(m1)
        every { matchDetailsRemoteRepository.getMatchDetail(idMatch2) } returns Right(m2)
        every { matchDetailsRemoteRepository.getMatchDetail(idMatch3) } returns Right(m3)
        val matchesHistoryInteractor = MatchesHistoryInteractor(
                matchesIdRepository,
                matchDetailsRemoteRepository,
                matchDetailsLocalRepository,
                deckDecoderRepository
        )

        //WHEN
        val res = matchesHistoryInteractor.getMatches(idPlayer1)

        //THEN
        assertEquals(Right(matches), res)
    }

    @Test
    fun getMatches_should_limit_remote_calls_when_datas_are_already_saved() {
        //GIVEN
        every { matchesIdRepository.getMatchesId(idPlayer1) } returns Right(idsMatch)
        every { matchDetailsLocalRepository.getMatchDetailOrNull(idMatch1) } returns Right(m1)
        every { matchDetailsLocalRepository.getMatchDetailOrNull(idMatch2) } returns Right(null)
        every { matchDetailsLocalRepository.getMatchDetailOrNull(idMatch3) } returns Right(m3)
        every { matchDetailsLocalRepository.persist(any()) } returns Unit
        every { matchDetailsRemoteRepository.getMatchDetail(idMatch2) } returns Right(m2)
        val matchesHistoryInteractor = MatchesHistoryInteractor(
                matchesIdRepository,
                matchDetailsRemoteRepository,
                matchDetailsLocalRepository,
                deckDecoderRepository
        )

        //WHEN
        val res = matchesHistoryInteractor.getMatches(idPlayer1)

        //THEN
        assertEquals(Right(matches), res)
    }

}