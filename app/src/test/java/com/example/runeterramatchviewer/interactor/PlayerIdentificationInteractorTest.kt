package com.example.runeterramatchviewer.interactor

import arrow.core.Right
import com.example.runeterramatchviewer.repository.PlayerIdRemoteRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Test

class PlayerIdentificationInteractorTest {

    private var playerIdentificationRepository: PlayerIdRemoteRepository = mockk()

    private val idResult: String = "MOCK_ID_RESULT_FOR_REPOSITORY"
    private val someName: String = "SOME_NAME"
    private val someTag: String = "SOME_TAG"

    @Test
    fun should_return_repository_result_when_inputs_are_filled() {
        //GIVEN
        every { playerIdentificationRepository.getPlayerId(any(), any()) } returns Right(idResult)
        val playerIdentificationInteractor =
                PlayerIdentificationInteractor(playerIdentificationRepository)
        //WHEN
        val result = playerIdentificationInteractor.getPlayerId(someName, someTag)
        //THEN
        assertEquals(Right(idResult), result)
    }
}