package com.example.runeterramatchviewer.repository

import com.example.runeterramatchviewer.model.*
import com.example.runeterramatchviewer.utils.ResourcesUtils
import io.mockk.every
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import no.stelar7.api.r4j.pojo.lor.offline.card.LoRCard
import no.stelar7.api.r4j.pojo.lor.offline.card.LoRDeck
import org.junit.Before
import org.junit.Test
import java.io.File

class DeckDecoderJsonReaderRepositoryTest {

    private val resourcesUtils = mockk<ResourcesUtils>()
    private val decoder = mockk<(String) -> LoRDeck>()
    private val pathJson = "src${File.separator}test${File.separator}resources${File.separator}set3-en_us.json"

    @Before
    fun setup() {
        every { resourcesUtils.getFile(any()) } returns File(pathJson)
                .inputStream()
                .readBytes()
                .toString(Charsets.UTF_8)
    }

    @Test
    fun decode_should_retreive_cards_in_json() {
        //GIVEN
        val card1 = LoRCard.create("03NX019")
        val card2 = LoRCard.create("03NX008")
        val lorDeck = LoRDeck()
        lorDeck.addCard(card1, 3)
        lorDeck.addCard(card2, 3)
        every { decoder(any()) } returns lorDeck
        val deckDecoderJsonReaderRepository = DeckDecoderJsonReaderRepository(resourcesUtils, decoder)

        //WHEN
        val res = deckDecoderJsonReaderRepository.decode("0000")

        //THEN
        val expected = Deck(mapOf(
                Pair(Card("Hunt the Weak", RegionName.NOXUS, TypeCardName.SPELL, RarityName.RARE), 3),
                Pair(Card("Heavy Blade Fragment", RegionName.NOXUS, TypeCardName.SPELL, RarityName.NONE), 3)))
        assertEquals(expected, res)
    }

    @Test
    fun decode_should_put_placeholder_if_not_found_in_json() {
        //GIVEN
        val card = LoRCard.create("03MT059")
        val lorDeck = LoRDeck()
        lorDeck.addCard(card, 3)
        every { decoder(any()) } returns lorDeck
        val deckDecoderJsonReaderRepository = DeckDecoderJsonReaderRepository(resourcesUtils, decoder)

        //WHEN
        val res = deckDecoderJsonReaderRepository.decode("0000")

        //THEN
        val expected = Deck(mapOf(Pair(
                Card("??", RegionName.UNKNOWN, TypeCardName.NONE, RarityName.NONE), 3)))
        assertEquals(expected, res)
    }
}