package com.example.runeterramatchviewer.repository

import android.content.Context
import com.example.runeterramatchviewer.model.*
import io.mockk.every
import io.mockk.mockk
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class MatchDetailsRemoteRepositoryTest {

    private val context = mockk<Context>()

    private val mockWebServer = MockWebServer()
    private val port = 8080
    private val pathFileMatchCode200 = "get_match_detail_200.json"
    private val idMatch = "a84074a8-eaaa-4bc7-86cc-43922f23f918"
    private val idWinner =
            "GKptQA5wE_2nBaHdYDqoFT8MaN3i3IERHFwEBmW-_KuCo7OaZ48iJzwRSv0LOfQ1LkKF77gTQzZ-2A"
    private val idLoser =
            "LHQUONcv-Juxh3PeO0VtUiYUzWMQ4J9jvASxrMZFM1-MwwfzhTpxqZU_cDDO4cz4iJtOm2ysgcljHA"
    private val mode = ModeName.CONSTRUCTED
    private val type = TypeMatchName.RANKED
    private val region1Winner = RegionName.DEMACIA
    private val region2Winner = RegionName.FRELJORD
    private val region1Loser = RegionName.IONIA
    private val region2Loser = RegionName.TARGON
    private val deckCodeWinner =
            "CIAQCAIACMBQCAQBAQBACABSGMEACAIBBMJBMKBKFY3QKAICAACQEAIAA4WAEAQBAIEAEAYBAIFQQAIBAUERGGBFEYUTQ"
    private val deckCodeLoser = "CIBQEAICBEYQGAQCAMDASBQDBEJRWIZJGNOAGAICAIEACAYCCQAQGCLCAEAQGCKV"
    private val totalTurn = 48
    private val dateStart = "2020-12-06T12:19:54.2164632+00:00"
    private val winner = PlayerDetail(idWinner, listOf(region1Winner, region2Winner), deckCodeWinner)
    private val loser = PlayerDetail(idLoser, listOf(region1Loser, region2Loser), deckCodeLoser)
    private val matchExpected = MatchDetail(idMatch, mode, type, winner, loser, totalTurn, dateStart)

    private val pathFilePlayerCode200 = "get_player_name_200.json"
    private val idPlayer =
            "LHQUONcv-Juxh3PeO0VtUiYUzWMQ4J9jvASxrMZFM1-MwwfzhTpxqZU_cDDO4cz4iJtOm2ysgcljHA"
    private val playerInfoExpected = PlayerInfo(idPlayer, "SorreIl")

    private val pathResource = "src${File.separator}test${File.separator}resources${File.separator}"
    private val cacheDirectory = "cache"

    @Before
    fun setup() {
        mockWebServer.start(port)
        val cachePath = Paths.get("$pathResource$cacheDirectory")
        Files.createDirectories(cachePath)
        every { context.cacheDir } returns File(cachePath.toUri())
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
        File("$pathResource$cacheDirectory").deleteRecursively()
    }

    @Test
    fun getMatchDetail_should_return_the_right_model_from_json() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFileMatchCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        val matchDetailsRemoteRepository = MatchDetailsWebRepository(retrofit)

        //WHEN
        val res = matchDetailsRemoteRepository.getMatchDetail(idMatch)

        //THEN
        assertEquals(true, res.isRight())
        assertEquals(matchExpected, res.fold({ null }, { it }))
    }

    @Test
    fun getMatchDetail_should_return_NetworkError_when_server_is_down() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(500)
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        val matchDetailsRemoteRepository = MatchDetailsWebRepository(retrofit)

        //WHEN
        val res = matchDetailsRemoteRepository.getMatchDetail(idMatch)

        //THEN
        assertEquals(true, res.isLeft())
        assertEquals(UnprocessedNetworkError, res.fold({ it }, { null }))
    }

    @Test
    fun getMatchDetail_should_return_the_model_in_cache() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFileMatchCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        val matchDetailsRemoteRepository = MatchDetailsWebRepository(retrofit)


        //WHEN
        matchDetailsRemoteRepository.getMatchDetail(idMatch) //response saved in cache
        val res = matchDetailsRemoteRepository.getMatchDetail(idMatch)

        //THEN
        assertEquals(true, res.isRight())
        assertEquals(matchExpected, res.fold({ null }, { it }))
        assertEquals(1, mockWebServer.requestCount)
    }

    @Test
    fun getPlayerName_should_return_the_right_model_from_json() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFilePlayerCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        val matchDetailsRemoteRepository = MatchDetailsWebRepository(retrofit)

        //WHEN
        val res = matchDetailsRemoteRepository.getPlayerName(idPlayer)

        //THEN
        assertEquals(true, res.isRight())
        assertEquals(playerInfoExpected, res.fold({ null }, { it }))
    }

    @Test
    fun getPlayerName_should_return_NetworkError_when_server_is_down() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(500)
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        val matchDetailsRemoteRepository = MatchDetailsWebRepository(retrofit)

        //WHEN
        val res = matchDetailsRemoteRepository.getPlayerName(idPlayer)

        //THEN
        assertEquals(true, res.isLeft())
        assertEquals(UnprocessedNetworkError, res.fold({ it }, { null }))
    }

    @Test
    fun getPlayerName_should_return_the_model_in_cache() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFilePlayerCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        val matchDetailsRemoteRepository = MatchDetailsWebRepository(retrofit)


        //WHEN
        matchDetailsRemoteRepository.getPlayerName(idPlayer) //response saved in cache
        val res = matchDetailsRemoteRepository.getPlayerName(idPlayer)

        //THEN
        assertEquals(true, res.isRight())
        assertEquals(playerInfoExpected, res.fold({ null }, { it }))
        assertEquals(1, mockWebServer.requestCount)
    }

    private fun read(path: String): String {
        return File(ClassLoader.getSystemResource(path).file).readText()
    }
}