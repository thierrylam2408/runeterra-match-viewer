package com.example.runeterramatchviewer.repository

import android.content.Context
import arrow.core.Either
import com.example.runeterramatchviewer.model.*
import io.mockk.every
import io.mockk.mockk
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths


class PlayerIdentificationRemoteRepositoryTest {

    private val context = mockk<Context>()
    private val networkManager = mockk<NetworkManager>()

    private val wrongName: String = "Wrong_name"
    private val wrongTag: String = "Wrong_tag"
    private val pathFileCode200 = "get_player_id_200.json"
    private val correctId =
            "LHQUONcv-Juxh3PeO0VtUiYUzWMQ4J9jvASxrMZFM1-MwwfzhTpxqZU_cDDO4cz4iJtOm2ysgcljHA"
    private val pathFileCode404 = "get_player_id_404.json"

    private val mockWebServer = MockWebServer()
    private val port = 8080

    private val pathResource = "src${File.separator}test${File.separator}resources${File.separator}"
    private val cacheDirectory = "cache"

    @Before
    fun setup() {
        mockWebServer.start(port)
        val cachePath = Paths.get("$pathResource$cacheDirectory")
        Files.createDirectories(cachePath)
        every { context.cacheDir } returns File(cachePath.toUri())
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
        File("$pathResource$cacheDirectory").deleteRecursively()
    }

    @Test
    fun getPlayerId_should_return_id_if_service_found_player() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFileCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        every { networkManager.isNetworkAvailable() } returns true
        val playerIdentificationRemoteRepository = PlayerIdRemoteRepository(retrofit, networkManager)

        //WHEN
        val res: Either<AppError, String> =
                playerIdentificationRemoteRepository.getPlayerId("name", "tag")

        //THEN
        assertEquals(true, res.isRight())
        assertEquals(correctId, res.fold({ null }, { it }))
    }


    @Test
    fun getPlayerId_should_return_PlayerNotFound_with_wrong_credentials() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(404)
                        .setBody(read(pathFileCode404))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        every { networkManager.isNetworkAvailable() } returns true
        val playerIdentificationRemoteRepository = PlayerIdRemoteRepository(retrofit, networkManager)

        //WHEN
        val res: Either<AppError, String> =
                playerIdentificationRemoteRepository.getPlayerId(wrongName, wrongTag)

        //THEN
        assertEquals(true, res.isLeft())
        assertEquals(HttpError(PLAYER_NOT_FOUND_CODE), res.fold({ it }, { null }))
    }

    @Test
    fun getPlayerId_should_return_NetworkError_when_server_is_down() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(500)
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        every { networkManager.isNetworkAvailable() } returns true
        val playerIdentificationRemoteRepository = PlayerIdRemoteRepository(retrofit, networkManager)

        //WHEN
        val res: Either<AppError, String> =
                playerIdentificationRemoteRepository.getPlayerId("name", "tag")

        //THEN
        assertEquals(true, res.isLeft())
        assertEquals(UnprocessedNetworkError, res.fold({ it }, { null }))
    }

    @Test
    fun getPlayerId_should_return_NoNetwork_when_device_has_no_connection() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFileCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        every { networkManager.isNetworkAvailable() } returns false
        val playerIdentificationRemoteRepository = PlayerIdRemoteRepository(retrofit, networkManager)

        //WHEN
        val res: Either<AppError, String> =
                playerIdentificationRemoteRepository.getPlayerId("name", "tag")

        //THEN
        assertEquals(true, res.isLeft())
        assertEquals(NoNetworkError, res.fold({ it }, { null }))
    }

    @Test
    fun getPlayerId_should_return_model_in_cache() {
        //GIVEN
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(read(pathFileCode200))
        )
        val url = "http://localhost:$port"
        val retrofit = RetrofitWrapper(url, context)
        every { networkManager.isNetworkAvailable() } returns true
        val playerIdentificationRemoteRepository = PlayerIdRemoteRepository(retrofit, networkManager)

        //WHEN
        playerIdentificationRemoteRepository
                .getPlayerId("name", "tag") //fill the cache
        val res: Either<AppError, String> =
                playerIdentificationRemoteRepository.getPlayerId("name", "tag")

        //THEN
        assertEquals(true, res.isRight())
        assertEquals(correctId, res.fold({ null }, { it }))
        assertEquals(1, mockWebServer.requestCount)
    }

    private fun read(path: String): String {
        return File(ClassLoader.getSystemResource(path).file).readText()
    }
}